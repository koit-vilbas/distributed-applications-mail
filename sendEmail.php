<?php

require_once 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 0;
//$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"]) && !empty($_FILES["fileToUpload"]["tmp_name"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $uploadOk = 0;
    }
}

if ($uploadOk == 1) {
    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
}


$mail = new PHPMailer(true);

$title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);

ob_start();
include "email.php";
$template = ob_get_clean();
//
//echo $template;
//die();

try {
    $mail->isSMTP();

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tls';
    $mail->Username = 'koit.vilbas@gmail.com';
    $mail->Password = 'chcouabezylxjswk';
    $mail->Port = 587;

    $mail->setFrom('koit.vilbas@gmail.com', 'Koit Vilbas');

    $mail->addAddress($email, explode('@', $email)[0]);

    $mail->Subject = $title;
    $mail->isHTML(true);
    $mail->AddEmbeddedImage('img/header.jpg', 'headerimg', 'header.jpg');
    $mail->Body = $template;

    $mail->send();
    $mail->clearAddresses();
    $mail->clearAllRecipients();
    $mail->clearAttachments();

    $mail->setFrom('koit.vilbas@gmail.com', 'Mail Form');
    $mail->addAddress('koit.vilbas@gmail.com', 'Koit Vilbas');

    $mail->Subject = $email . ' : '. $title;
    $mail->Body = $message;

    if($uploadOk) {
        $mail->addAttachment($target_file, basename( $_FILES["fileToUpload"]["name"]));
    }

    $mail->send();

    header('Location: /success.html');
} catch (Exception $exception) {
    echo $exception->errorMessage();
    header('Location: /error.html');
} catch (\Exception $e)
{
    echo $e->getMessage();
    header('Location: /error.html');
}