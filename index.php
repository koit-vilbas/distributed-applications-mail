<?php
require_once 'vendor/autoload.php';
?>

<html lang="en">
 <head>
     <title>Mailer</title>
     <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet">
 </head>
<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <form method="post" action="sendEmail.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp" placeholder="Enter title">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" name="message" id="message"></textarea>
                    </div>
                    <div class="custom-file">
                        <input type="file" name="fileToUpload" class="custom-file-input" id="customFileLang" lang="es">
                        <label class="custom-file-label" for="customFileLang">Select file</label>
                    </div>

                    <button type="submit" name="submit" class="btn btn-primary my-2">Submit</button>
                </form>
            </div>
        </div>
    </div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
